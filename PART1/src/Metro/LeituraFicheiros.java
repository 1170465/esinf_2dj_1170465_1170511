/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ANDRÉ
 */
public class LeituraFicheiros {

    public static void lerEstacoes(Linha l) throws FileNotFoundException {
        Scanner in = new Scanner(new FileReader("fx_estacoes.txt"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(",");
                l.addEstacao(temp[1], Integer.parseInt(temp[0]), temp[2]);
            }
        }
        l.reverseLinha();
        in.close();

    }

    public static void lerViagens(Linha l) throws FileNotFoundException {
        Scanner in = new Scanner(new FileReader("fx_viagens.txt"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(",");
                Bilhete b = new Bilhete(Integer.parseInt(temp[0]), temp[1],
                        Integer.parseInt(temp[2]), Integer.parseInt(temp[3]));
                l.addBilhete(b);
            }
        }
        in.close();
    }
}