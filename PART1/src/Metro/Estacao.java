/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ANDRÉ
 */
public class Estacao {
    
    private int numEstacao;
    private String descricao;
    private String zona;
    
    private int passagens;
    

    public Estacao(int numEstacao, String descricao, String zona) {
        this.numEstacao = numEstacao;
        this.descricao = descricao;
        this.zona = zona;
    }
    
    public Estacao(){
    }
    
    public int getNumEstacao() {
        return numEstacao;
    }

    public void setNumEstacao(int numEstacao) {
        this.numEstacao = numEstacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public int getPassagens() {
        return passagens;
    }

    public void setPassagens(int passagens) {
        this.passagens = passagens;
    }
    
    


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + this.numEstacao;
        hash = 13 * hash + Objects.hashCode(this.descricao);
        hash = 13 * hash + Objects.hashCode(this.zona);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        if (this.numEstacao != other.numEstacao) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.zona, other.zona)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estacao{" + "Número da estação: " + numEstacao + ", Descrição: " + descricao + ", Zona: " + zona + '}';
    }   
}
