/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import javafx.scene.Node;

/**
 *
 * @author ANDRÉ
 */
public class Linha {

    DoublyLinkedList listaEstacoes;
    private List<Bilhete> bilhetes;

    public Linha() {
        listaEstacoes = new DoublyLinkedList();
        bilhetes = new ArrayList<>();
    }

    public DoublyLinkedList getListaEstacoes() {
        return listaEstacoes;
    }

    public void setListaEstacoes(DoublyLinkedList listaEstacoes) {
        this.listaEstacoes = listaEstacoes;
    }

    public void addEstacao(String descricao, int numero, String zona) {
        listaEstacoes.addFirst(new Estacao(numero, descricao, zona));
    }

    public void reverseLinha() {
        listaEstacoes.reverse();
    }

    public List<Bilhete> getListBilhetes() {
        return bilhetes;
    }

    public void addBilhete(Bilhete b) {
        bilhetes.add(b);
    }

    public Estacao getEstacaoByNumber(int num) {
        return listaEstacoes.procurarPorEstacao(num);
    }

    public int numeroPassagensNaEstacao(int num) {
        Estacao e = listaEstacoes.procurarPorEstacao(num);
        int soma = 0;
        for (Bilhete b : bilhetes) {
            int o = b.getEstOrigem();
            int d = b.getEstDestino();
            if (o > d) {
                int temp = o;
                o = d;
                d = temp;
            }
            for (int i = o; i < d + 1; i++) {
                if (i == e.getNumEstacao()) {
                    soma++;
                }
            }
        }
        return soma;
    }

    public List<Bilhete> ifTrangressao() {
        List<Bilhete> bilhetesTransgressao = new ArrayList<>();
        for (Bilhete b : bilhetes) {
            int o = b.getEstOrigem();
            int d = b.getEstDestino();
            String zona = b.getTipo();
            String[] aux = zona.split("Z");
            int numZona = Integer.parseInt(aux[1]);
            if (o > d) {
                int temp = o;
                o = d;
                d = temp;
            }
            String aux2 = "";
            int soma = 0;
            for (int i = o; i < d + 1; i++) {
                Estacao e = listaEstacoes.procurarPorEstacao(i);
                if (!aux2.equals(e.getZona())) {
                    aux2 = e.getZona();
                    soma++;
                }
            }
            if (soma > numZona) {
                bilhetesTransgressao.add(b);
            }
        }
        return bilhetesTransgressao;
    }

    public List<Estacao> getMaiorSequenciaEstacoes(String tipoBilhete) {
        List<String> zonasPassadas = new ArrayList<>();
        List<Estacao> tmp = new ArrayList<>();
        List<String> zonasP = new ArrayList<>();

        int numZonas = 0;
        int cont = 0;

        if (tipoBilhete.equals("Z2")) {
            numZonas = 2;
        } else if (tipoBilhete.equals("Z3")) {
            numZonas = 3;
        } else if (tipoBilhete.equals("Z4")) {
            numZonas = 4;
        } else if (tipoBilhete.equals("Z5")) {
            numZonas = 5;
        } else {
            throw new RuntimeException("Não existem bilhetes que tenham acesso a 6 zonas.");
        }

        int zonas = numZonas(numZonas, cont, tmp);
        int nrEst = -1;
        List<Estacao> aux = new ArrayList<>();
        for (int i = 0; i < zonas - 2; i++) {
            if (aux.size() > nrEst - 1) {
                tmp = sequencia(numZonas, cont, tmp, zonasPassadas, zonasP);
                nrEst = tmp.size();
                if (aux.size() < tmp.size()) {
                    aux.clear();
                    aux.addAll(tmp);
                }
                tmp.clear();
            }
        }
        return aux;
    }

    public List<Estacao> sequencia(int numZonas, int cont, List<Estacao> tmp, List<String> zonasPassadas, List<String> zonas) {
        int size = listaEstacoes.size();
        if (zonasPassadas.size() > 0) {
            zonas.remove(zonas.size() - 1);
            zonas.remove(zonas.size() - 1);
            zonasPassadas.clear();
        }
        for (int i = 0; i < size; i++) {
            Estacao estacao = (Estacao) listaEstacoes.procurarPorEstacao(i + 1);;
            while (cont <= numZonas) {
                if (!zonas.contains(estacao.getZona())) {
                    zonasPassadas.add(estacao.getZona());
                    zonas.add(estacao.getZona());
                    cont++;
                }
                if (zonasPassadas.contains(estacao.getZona())) {
                    tmp.add(estacao);
                }
                break;
            }
        }
        zonasPassadas.remove(1);
        tmp.remove(tmp.size() - 1);
        return tmp;
    }

    public int numZonas(int numZonas, int cont, List<Estacao> tmp) {
        List<String> zonasPassadas = new ArrayList<>();
        int num = 0;
        int size = listaEstacoes.size();
        for (int i = 0; i < size; i++) {
            Estacao estacao = listaEstacoes.procurarPorEstacao(i + 1);
            if (!zonasPassadas.contains(estacao.getZona())) {
                zonasPassadas.add(estacao.getZona());
                num++;
            }
        }
        return num;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.listaEstacoes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linha other = (Linha) obj;
        if (!Objects.equals(this.listaEstacoes, other.listaEstacoes)) {
            return false;
        }
        return true;
    }
}
