/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 *
 * @author ANDRÉ
 */
public class DoublyLinkedList<E> implements Iterable<E> {

    public final Node<E> trailer;
    Node<E> header;
    private int size = 0;
    private int modCount = 0;

    public DoublyLinkedList() {
        header = new Node<>(null, null, null);
        trailer = new Node<>(null, header, null);
        header.setNext(trailer);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    public E first() {
        if (size != 0) {
            return header.next.element;
        } else {
            return null;
        }
    }

    public E last() {
        if (size != 0) {
            return trailer.prev.element;
        } else {
            return null;
        }
    }

    public void addFirst(E e) {
        addBetween(e, header, header.getNext());
    }

    public void addLast(E e) {
        addBetween(e, trailer.getPrev(), trailer);
    }

    private E remove(Node<E> node) {
        node.next.prev = node.prev;
        node.prev.next = node.next;
        size--;
        modCount++;
        return node.element;
    }

    public E removeFirst() {
        if (size != 0) {
            return remove(header.next);
        }
        return null;
    }

    public E removeLast() {
        if (size != 0) {
            return remove(trailer.prev);
        }
        return null;
    }

    private void addBetween(E e, Node<E> predecessor, Node<E> successor) {
        Node<E> node = new Node<>(e, predecessor, successor);
        successor.prev = node;
        predecessor.next = node;
        size++;
        modCount++;
    }

    public void reverse() {
        Node temp = null;
        Node current = header;

        /* swap next and prev for all nodes of  
         doubly linked list */
        while (current != null) {
            temp = current.prev;
            current.prev = current.next;
            current.next = temp;
            current = current.prev;
        }

        /* Before changing head, check for the cases like empty  
         list and list with only one node */
        if (temp != null) {
            header = temp.prev;
        }
    }

    public Estacao procurarPorEstacao(int num) {
        ListIterator<E> it = (ListIterator<E>) iterator();
        Estacao est = null;
        if (!isEmpty()) {
            if (it.hasNext()) {
                Estacao est2 = (Estacao) it.next();
                if (est2 != null) {
                    if (est2.getNumEstacao() == num) {
                        est = est2;
                    } else {
                        return procurarPorEstacao(num, it);
                    }
                }
            }
        }
        return est;
    }

    public Estacao procurarPorEstacao(int num, ListIterator<E> it) {
        if (it.hasNext()) {
            Estacao est = (Estacao) it.next();
            if (est != null) {
                if (est.getNumEstacao() == num) {
                    return est;
                } else {
                    return procurarPorEstacao(num, it);
                }
            }
        } else {
            return null;
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.trailer);
        hash = 79 * hash + Objects.hashCode(this.header);
        hash = 79 * hash + this.size;
        hash = 79 * hash + this.modCount;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DoublyLinkedList<?> other = (DoublyLinkedList<?>) obj;
        if (this.size != other.size) {
            return false;
        }
        if (this.modCount != other.modCount) {
            return false;
        }
        if (!Objects.equals(this.trailer, other.trailer)) {
            return false;
        }
        if (!Objects.equals(this.header, other.header)) {
            return false;
        }
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoublyLinkedList clone = new DoublyLinkedList<>();
        ListIterator iterator = this.listIterator();
        while (iterator.hasNext()) {
            clone.addLast(iterator.next());

        }
        return clone;
    }

    private class DoublyLinkedListIterator implements ListIterator<E> {

        private DoublyLinkedList.Node<E> nextNode, prevNode, lastReturnedNode;
        private int nextIndex;
        private int expectedModCount;

        public DoublyLinkedListIterator() {
            this.prevNode = header;
            this.nextNode = header.getNext();
            lastReturnedNode = null;
            nextIndex = 0;
            expectedModCount = modCount;
        }

        final void checkForComodification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public boolean hasNext() {
            return nextNode != trailer;
        }

        @Override
        public E next() throws NoSuchElementException {
            checkForComodification();

            if (nextNode != trailer) {
                prevNode = nextNode;
                nextNode = nextNode.getNext();
                lastReturnedNode = prevNode;
                nextIndex++;
                return prevNode.getElement();
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public boolean hasPrevious() {
            return prevNode != header;
        }

        @Override
        public E previous() throws NoSuchElementException {
            checkForComodification();
            if (prevNode != header) {
                nextNode = prevNode;
                prevNode = prevNode.getPrev();
                lastReturnedNode = nextNode;
                nextIndex--;
                return nextNode.getElement();
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() throws NoSuchElementException {
            if (lastReturnedNode == null || DoublyLinkedList.this.isEmpty()) {
                throw new NoSuchElementException();
            }
            checkForComodification();

            prevNode = lastReturnedNode.getPrev();
            nextNode = lastReturnedNode.getNext();
            DoublyLinkedList.this.remove(lastReturnedNode);
            expectedModCount++;
        }

        @Override
        public void set(E e) throws NoSuchElementException {
            if (lastReturnedNode == null) {
                throw new NoSuchElementException();
            }
            checkForComodification();

            lastReturnedNode.setElement(e);
        }

        @Override
        public void add(E e) {
            checkForComodification();
            DoublyLinkedList.this.addBetween(e, prevNode, nextNode);
            expectedModCount++;
            prevNode = nextNode.getPrev();
        }    
    }

    @Override
    public Iterator<E> iterator() {
        return new DoublyLinkedListIterator();
    }

    public ListIterator<E> listIterator() {
        return new DoublyLinkedListIterator();
    }

    private static class Node<E> {

        private E element;
        private Node<E> prev;
        private Node<E> next;

        public Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setElement(E element) { // Not on the original interface. Added due to list iterator implementation
            this.element = element;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

    }

}
