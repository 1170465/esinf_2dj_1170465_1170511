/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro;

import java.util.Objects;

/**
 *
 * @author ANDRÉ
 */
public class Bilhete {
    
    private int num;
    private String tipo;
    private int estOrigem;
    private int estDestino;

    public Bilhete(int num, String tipo, int estOrigem, int estDestino) {
        this.num = num;
        this.tipo = tipo;
        this.estOrigem = estOrigem;
        this.estDestino = estDestino;
    }

    public Bilhete() {
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getEstOrigem() {
        return estOrigem;
    }

    public void setEstOrigem(int estOrigem) {
        this.estOrigem = estOrigem;
    }

    public int getEstDestino() {
        return estDestino;
    }

    public void setEstDestino(int estDestino) {
        this.estDestino = estDestino;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.num;
        hash = 37 * hash + Objects.hashCode(this.tipo);
        hash = 37 * hash + this.estOrigem;
        hash = 37 * hash + this.estDestino;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bilhete other = (Bilhete) obj;
        if (this.num != other.num) {
            return false;
        }
        if (this.estOrigem != other.estOrigem) {
            return false;
        }
        if (this.estDestino != other.estDestino) {
            return false;
        }
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bilhete{" + "Número: " + num + ", Tipo: " + tipo + ", Estação origem: " + estOrigem + ", Estação destino: " + estDestino + '}';
    }  
}
