package Testes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Metro.Bilhete;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Utilizador
 */
public class BilheteTest {
    
    public BilheteTest(){
    }
    
    @Test
    public void testBilhete(){
        Bilhete b1 = new Bilhete(111222333,"Z2",1,10);
        assertEquals(b1,new Bilhete(111222333,"Z2",1,10));
    }
    
    @Test
    public void testBilheteClear(){
        Bilhete b2 = new Bilhete();
        assertEquals(b2,new Bilhete());
    }
    
    @Test
    public void testSetNum(){
        Bilhete b3 = new Bilhete(111222334,"Z2",1,10);
        b3.setNum(1);
        assertEquals(b3.getNum(),1);
    }
    
    @Test
    public void testSetTipo(){
        Bilhete b4 = new Bilhete(111222334,"Z3",1,10);
        b4.setTipo("Z4");
        assertEquals(b4.getTipo(),"Z4");
    }
    
    @Test
    public void testSetEstOrigem(){
        Bilhete b5 = new Bilhete(111222335,"Z2",1,10);
        b5.setEstOrigem(2);
        assertEquals(b5.getEstOrigem(),2);
    }
    
    @Test
    public void testSetEstDestino(){
        Bilhete b6 = new Bilhete(111222336,"Z2",1,10);
        b6.setEstOrigem(9);
        assertEquals(b6.getEstOrigem(),9);
    }
    
    @Test
    public void testHashCode(){
        Bilhete b7 = new Bilhete(111222337,"Z2",1,10);
        assertEquals(b7.hashCode(),-1242545801);
    }

}
