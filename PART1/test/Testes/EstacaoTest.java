/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import Metro.Estacao;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class EstacaoTest {
    public EstacaoTest(){
    }
    
    @Test
    public void testEstacao(){
        Estacao e1 = new Estacao(2,"IPO","C1");
        assertEquals(e1,new Estacao(2,"IPO","C1"));
    }
    
    @Test
    public void testEstacaoClear(){
        Estacao e2 = new Estacao();
        assertEquals(e2,new Estacao());
    }
    
    @Test
    public void testSetNumEstacao(){
        Estacao e3 = new Estacao(3,"IPO","C1");
        e3.setNumEstacao(1);
        assertEquals(e3.getNumEstacao(),1);
    }
    
    @Test
    public void testSetDescricao(){
        Estacao e4 = new Estacao(2,"HOSPITAL","C1");
        e4.setDescricao("ALIADOS");
        assertEquals(e4.getDescricao(),"ALIADOS");
    }
    
    @Test
    public void testSetZona(){
        Estacao e5 = new Estacao(2,"IPO","C2");
        e5.setZona("C3");
        assertEquals(e5.getZona(),"C3");
    }
    
    @Test
    public void testHashCode(){
        Estacao e7 = new Estacao(2,"TRINDADE","C1");
        assertEquals(e7.hashCode(),-267579316);
    }
}
