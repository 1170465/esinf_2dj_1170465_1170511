/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import Metro.Bilhete;
import Metro.DoublyLinkedList;
import Metro.Estacao;
import Metro.LeituraFicheiros;
import Metro.Linha;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javafx.scene.Node;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class LinhaTest {

    @Test
    public void getListaEstacoes() {
        Linha l = new Linha();
        List<Estacao> lste = new ArrayList<>();
        Estacao e = new Estacao(1, "A", "C1");
        Estacao e2 = new Estacao(2, "B", "C2");
        lste.add(e);
        lste.add(e2);
        l.getListaEstacoes().addLast(e);
        l.getListaEstacoes().addLast(e2);
        int size = l.getListaEstacoes().size();
        for (int i = 0; i < size; i++) {
            Estacao a = (Estacao) l.getListaEstacoes().iterator().next();
            assertEquals(lste.get(i), a);
            l.getListaEstacoes().removeFirst();
        }
    }

    @Test
    public void numeroPassagensNaEstacaoTest() throws FileNotFoundException {
        Linha l = new Linha();
        List<Bilhete> b = new ArrayList<>();
        Bilhete b1 = new Bilhete(111222333, "Z2", 1, 13);
        Bilhete b2 = new Bilhete(111333222, "Z3", 1, 22);
        Bilhete b3 = new Bilhete(111444222, "Z2", 22, 5);
        b.add(b1);
        b.add(b2);
        b.add(b3);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(3, l.numeroPassagensNaEstacao(10));
        System.out.println("Na estação número 10 passaram "+ l.numeroPassagensNaEstacao(10) + " pessoas.");
    }

    @Test
    public void numeroPassagensNaEstacaoTest1() throws FileNotFoundException {
        Linha l = new Linha();
        List<Bilhete> b = new ArrayList<>();
        Bilhete b1 = new Bilhete(111222333, "Z2", 1, 13);
        Bilhete b2 = new Bilhete(111333222, "Z3", 1, 22);
        Bilhete b3 = new Bilhete(111444222, "Z2", 22, 5);
        b.add(b1);
        b.add(b2);
        b.add(b3);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(3, l.numeroPassagensNaEstacao(2));
        System.out.println("Na estação número 2 passaram "+ l.numeroPassagensNaEstacao(2) + " pessoas.");
    }

    @Test
    public void numeroPassagensNaEstacaoTest2() throws FileNotFoundException {
        Linha l = new Linha();
        List<Bilhete> b = new ArrayList<>();
        Bilhete b1 = new Bilhete(111222333, "Z2", 1, 13);
        Bilhete b2 = new Bilhete(111333222, "Z3", 1, 22);
        Bilhete b3 = new Bilhete(111444222, "Z2", 22, 5);
        b.add(b1);
        b.add(b2);
        b.add(b3);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(0, l.numeroPassagensNaEstacao(23));
        System.out.println("Na estação número 0 passaram "+ l.numeroPassagensNaEstacao(23) + " pessoas.");
    }

    @Test
    public void ifTrangressao() throws FileNotFoundException {
        Linha l = new Linha();
        List<Bilhete> b = new ArrayList<>();
        List<Bilhete> transgressao = new ArrayList<>();
        Bilhete b1 = new Bilhete(111222333, "Z2", 1, 12);
        Bilhete b2 = new Bilhete(111333222, "Z3", 1, 22);
        Bilhete b3 = new Bilhete(111444222, "Z2", 22, 5);
        b.add(b1);
        b.add(b2);
        b.add(b3);
        transgressao.add(b2);
        transgressao.add(b3);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(transgressao, l.ifTrangressao());
        System.out.println(l.ifTrangressao());
    }

    @Test
    public void getMaiorSequenciaEstacoesTest() throws FileNotFoundException {
        Linha l = new Linha();
        Bilhete b1 = new Bilhete(111222333, "Z2", 1, 12);
        Estacao e1 = new Estacao(5, "Combatentes", "C1");
        Estacao e2 = new Estacao(6, "Marques", "C1");
        Estacao e3 = new Estacao(7, "Faria Guimaraes", "C1");
        Estacao e4 = new Estacao(8, "Trindade", "C1");
        Estacao e5 = new Estacao(9, "Lapa", "C1");
        Estacao e6 = new Estacao(10, "Carolina Micaelis", "C1");
        Estacao e7 = new Estacao(11, "Casa da Musica", "C1");
        Estacao e8 = new Estacao(12, "Francos", "C1");
        Estacao e9 = new Estacao(13, "Ramalde", "C2");
        Estacao e10 = new Estacao(14, "Viso", "C2");
        Estacao e11 = new Estacao(15, "Sete Bicas", "C2");
        Estacao e12 = new Estacao(16, "Senhora da Hora", "C2");
        Estacao e13 = new Estacao(17, "A", "C2");
        List<Estacao> e = new ArrayList<>();
        e.add(e1);
        e.add(e2);
        e.add(e3);
        e.add(e4);
        e.add(e5);
        e.add(e6);
        e.add(e7);
        e.add(e8);
        e.add(e9);
        e.add(e10);
        e.add(e11);
        e.add(e12);
        e.add(e13);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(e, l.getMaiorSequenciaEstacoes(b1.getTipo()));
    }

    @Test
    public void getMaiorSequenciaEstacoesTest1() throws FileNotFoundException {
        Linha l = new Linha();
        Bilhete b1 = new Bilhete(111222333, "Z3", 1, 12);
        Estacao e1 = new Estacao(5, "Combatentes", "C1");
        Estacao e2 = new Estacao(6, "Marques", "C1");
        Estacao e3 = new Estacao(7, "Faria Guimaraes", "C1");
        Estacao e4 = new Estacao(8, "Trindade", "C1");
        Estacao e5 = new Estacao(9, "Lapa", "C1");
        Estacao e6 = new Estacao(10, "Carolina Micaelis", "C1");
        Estacao e7 = new Estacao(11, "Casa da Musica", "C1");
        Estacao e8 = new Estacao(12, "Francos", "C1");
        Estacao e9 = new Estacao(13, "Ramalde", "C2");
        Estacao e10 = new Estacao(14, "Viso", "C2");
        Estacao e11 = new Estacao(15, "Sete Bicas", "C2");
        Estacao e12 = new Estacao(16, "Senhora da Hora", "C2");
        Estacao e13 = new Estacao(17, "A", "C2");
        Estacao e14 = new Estacao(1,"Hospital de S.Joao","C6");
        Estacao e15 = new Estacao(2,"IPO","C6");
        Estacao e16 = new Estacao(3,"Polo Universitario","C6");
        Estacao e17 = new Estacao(4,"Salgueiros","C6");
        List<Estacao> e = new ArrayList<>();
        e.add(e14);
        e.add(e15);
        e.add(e16);
        e.add(e17);
        e.add(e1);
        e.add(e2);
        e.add(e3);
        e.add(e4);
        e.add(e5);
        e.add(e6);
        e.add(e7);
        e.add(e8);
        e.add(e9);
        e.add(e10);
        e.add(e11);
        e.add(e12);
        e.add(e13);
        LeituraFicheiros.lerEstacoes(l);
        LeituraFicheiros.lerViagens(l);
        assertEquals(e, l.getMaiorSequenciaEstacoes(b1.getTipo()));
    }
}
