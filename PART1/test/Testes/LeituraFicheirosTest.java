/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import Metro.Bilhete;
import Metro.Estacao;
import Metro.LeituraFicheiros;
import Metro.Linha;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class LeituraFicheirosTest {

    List<Estacao> e = new ArrayList<>();
    List<Bilhete> b = new ArrayList<>();

    public void lerEstacoes() throws FileNotFoundException {
        Scanner in = new Scanner(new FileReader("fx_estacoes.txt"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(",");
                Estacao estacao = new Estacao(Integer.parseInt(temp[0]), temp[1], temp[2]);
                e.add(estacao);
            }
        }
        in.close();
    }

    @Test
    public void lerEstacoesTest() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerEstacoes(l);
        assertEquals(23, l.getListaEstacoes().size());
    }

    @Test
    public void lerEstacoesTest2() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerEstacoes(l);
        assertNotNull(l.getListaEstacoes());
    }

    @Test
    public void lerEstacoesTest3() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerEstacoes(l);
        ListIterator aux = l.getListaEstacoes().listIterator();
        lerEstacoes();
        for (Estacao est : e) {
            assertEquals(est,aux.next());
        }
    }
    
    public void lerViagens() throws FileNotFoundException {
        Scanner in = new Scanner(new FileReader("fx_viagens.txt"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(",");
                Bilhete bil = new Bilhete(Integer.parseInt(temp[0]), temp[1],
                        Integer.parseInt(temp[2]), Integer.parseInt(temp[3]));
                b.add(bil);
            }
        }
        in.close();
    }

    @Test
    public void lerBilhetesTest() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerViagens(l);
        assertEquals(3, l.getListBilhetes().size());
    }

    @Test
    public void lerBilhetesTest2() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerViagens(l);
        assertNotNull(l.getListBilhetes());
    }
    
    @Test
    public void lerBilhetesTest3() throws FileNotFoundException {
        Linha l = new Linha();
        LeituraFicheiros.lerViagens(l);
        lerViagens();
        int i=0;
        for (Bilhete bilh : b) {
            assertEquals(bilh,l.getListBilhetes().get(i));
            i++;
        }
    }
}
