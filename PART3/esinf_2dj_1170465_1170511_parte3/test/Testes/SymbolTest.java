/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import PL.Symbol;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class SymbolTest {
    
    @Test
    public void newSymbolTest(){
        Symbol s = new Symbol();
        assertEquals(s, new Symbol());
    }
    
    @Test
    public void newSymbolTest2(){
        Symbol s = new Symbol("A","A","A");
        assertEquals(s, new Symbol("A","A","A"));
    }
    
    @Test
    public void getTipoTest(){
        Symbol s = new Symbol("A","B","C");
        assertEquals(s.getTipo(), "A");
    }
    
    @Test
    public void setTipoTest(){
        Symbol s = new Symbol("A","B","C");
        s.setTipo("D");
        assertEquals(s.getTipo(), "D");
    }
    
    @Test
    public void getSymbolTest(){
        Symbol s = new Symbol("A","B","C");
        assertEquals(s.getSymbol(), "B");
    }

    @Test
    public void setSymbolTest(){
        Symbol s = new Symbol("A","B","C");
        s.setSymbol("D");
        assertEquals(s.getSymbol(), "D");
    }
    
    @Test
    public void getCodeTest(){
        Symbol s = new Symbol("A","B","C");
        assertEquals(s.getCode(), "C");
    }
    
    @Test
    public void setCodeTest(){
        Symbol s = new Symbol("A","B","C");
        s.setCode("D");
        assertEquals(s.getCode(), "D");
    }
    
    @Test
    public void hashCodeTest(){
        Symbol s = new Symbol("A","B","C");
        assertEquals(s.hashCode(),54365);
    }
    
    @Test
    public void toStringTest(){
        Symbol s = new Symbol("A","B","C");
        assertEquals(s.toString(),"B");
    }
    
    @Test
    public void equalsTest(){
        Symbol s = new Symbol("A","B","C");
        assertFalse(s.equals(new Symbol("A","D","E")));
    }
    
    @Test
    public void equalsTest2(){
        Symbol s = new Symbol("A","B","C");
        assertFalse(s.equals(new Symbol("D","B","E")));
    }
    
    @Test
    public void equalsTest3(){
        Symbol s = new Symbol("A","B","C");
        assertFalse(s.equals(new Symbol("D","E","C")));
    }
    
    @Test
    public void equalsTest7(){
        Symbol s = new Symbol("A","B","C");
        assertFalse(s.equals(new Symbol("A","B","D")));
    }
    
    @Test
    public void equalsTest4(){
        Symbol s = new Symbol("A","B","C");
        String a = "";
        assertFalse(s.equals(a));
    }
    
    @Test
    public void equalsTest5(){
        Symbol s = new Symbol("A","B","C");
        assertFalse(s.equals(null));
    }
    
    @Test
    public void equalsTest6(){
        Symbol s = new Symbol("A","B","C");
        assertTrue(s.equals(s));
    }
}
