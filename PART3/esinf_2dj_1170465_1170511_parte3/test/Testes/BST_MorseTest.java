/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import PL.BST_Morse;
import PL.LeituraFicheiro;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class BST_MorseTest {
    
    // EX1e2
    @Test
    public void testDesccodificar() throws FileNotFoundException {
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        System.out.println("----ÁRVORE----\n");
        System.out.println(bst.toStringTree());
        System.out.println("");
        String a = "_._. ___ _.. .. __. ___/__ ___ ._. ... . ..__..";
        String s = bst.descCode(a);
        System.out.println("Frase: " + a);
        System.out.println("Descodificação: " + s + "\n");
        assertEquals("CODIGO MORSE?",s);
    }
    
    // EX 3e4
    @Test
    public void testCodificar() throws FileNotFoundException {
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        bst.createTreeLetter();
        System.out.println("----ÁRVORE LETRAS----\n");
        System.out.println(bst.toStringTreeLetter());
        System.out.println("");
        String a = "CODIGO MORSE";
        String s = bst.codCodeLetter(a);
        System.out.println("Frase: " + a);
        System.out.println("Codificação: " + s + "\n");
        assertEquals("_._. ___ _.. .. __. ___/__ ___ ._. ... .",s);
    }
    
    // EX5
    @Test
    public void testSequenciaComumInicial() throws FileNotFoundException{
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        String s = bst.sameSequence("5", "4");
        System.out.println("Sequencia inicial comum (5 e 4): " + s + "\n");
        assertEquals("....",s);
    }
    
    // EX5
    @Test
    public void testSequenciaComumInicial2() throws FileNotFoundException{
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        String s = bst.sameSequence("1", "B");
        System.out.println("Sequencia inicial comum (1 e B): " + s + "\n");
        assertEquals("Não existe sequência inicial comum aos dois caracteres indicados",s);
    }
    
    // EX6
    @Test
    public void testOrdenacaoPorClasseNumber() throws FileNotFoundException{
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        List<String> lst = new ArrayList<>();
        lst.add(".____ ..___ ...__"); //123
        lst.add("._ ..___ ...__");  //A23
        lst.add("._ _... ...__");   //AB3
        lst.add("._ _... _._.");    //ABC
        lst.add("_._.__ ..___ _._.");   //!2C
        lst.add("_._.__ ..__.. _._.");  //!?C
        lst.add("_._.__ ..__.. _..._"); //!?=
        List<String> lstS = bst.ordenacaoPorTipoSimbolo(lst, "Number");
        List<String> test = new ArrayList<>();
        test.add("123-3");
        test.add("A23-2");
        test.add("AB3-1");
        test.add("!2C-1");
        System.out.println("Ordenação por classe “Number”:" + lstS);
        assertEquals(test,lstS);
    }
    
    // EX6
    @Test
    public void testOrdenacaoPorClasseLetter() throws FileNotFoundException{
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        List<String> lst = new ArrayList<>();
        lst.add(".____ ..___ ...__"); //123
        lst.add("._ ..___ ...__");  //A23
        lst.add("._ _... ...__");   //AB3
        lst.add("._ _... _._.");    //ABC
        lst.add("_._.__ ..___ _._.");   //!2C
        lst.add("_._.__ ..__.. _._.");  //!?C
        lst.add("_._.__ ..__.. _..._"); //!?=
        List<String> lstS = bst.ordenacaoPorTipoSimbolo(lst, "Letter");
        List<String> test = new ArrayList<>();
        test.add("ABC-3");
        test.add("AB3-2");
        test.add("A23-1");
        test.add("!2C-1");
        test.add("!?C-1");
        System.out.println("Ordenação por classe “Letter”:" + lstS);
        assertEquals(test,lstS);
    }
    
    // EX6
    @Test
    public void testOrdenacaoPorClassePunctuation() throws FileNotFoundException{
        LeituraFicheiro lf = new LeituraFicheiro();
        BST_Morse bst = new BST_Morse();
        lf.leituraEstacao(bst);
        List<String> lst = new ArrayList<>();
        lst.add(".____ ..___ ...__"); //123
        lst.add("._ ..___ ...__");  //A23
        lst.add("._ _... ...__");   //AB3
        lst.add("._ _... _._.");    //ABC
        lst.add("_._.__ ..___ _._.");   //!2C
        lst.add("_._.__ ..__.. _._.");  //!?C
        lst.add("_._.__ ..__.. _..._"); //!?=
        List<String> lstS = bst.ordenacaoPorTipoSimbolo(lst, "Punctuation");
        List<String> test = new ArrayList<>();
        test.add("!?=-3");
        test.add("!?C-2");
        test.add("!2C-1");
        System.out.println("Ordenação por classe “Punctuation”:" + lstS);
        assertEquals(test,lstS);
    }
}
