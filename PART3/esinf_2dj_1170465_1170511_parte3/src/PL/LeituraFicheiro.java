/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Utilizador
 */
public class LeituraFicheiro {
    
    public void leituraEstacao(BST_Morse bst) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("morse_v3.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(" ");
                bst.insert(new Symbol(temp[2], temp[1], temp[0]));
            }
        }
        in.close();
    }
    
}
