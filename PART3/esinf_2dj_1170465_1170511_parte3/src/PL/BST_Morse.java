/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Utilizador
 * @param <E>
 */
public class BST_Morse<E extends Comparable<E>> {

    /**
     * Nested static class for a binary search tree node.
     */
    protected static class Node<E> {

        private E element;          // an element stored at this node
        private Node<E> left;       // a reference to the left child (if any)
        private Node<E> right;      // a reference to the right child (if any)

        /**
         * Constructs a node with the given element and neighbors.
         *
         * @param e the element to be stored
         * @param leftChild reference to a left child node
         * @param rightChild reference to a right child node
         */
        public Node(E e, Node<E> leftChild, Node<E> rightChild) {
            element = e;
            left = leftChild;
            right = rightChild;
        }

        // accessor methods
        public E getElement() {
            return element;
        }

        public Node<E> getLeft() {
            return left;
        }

        public Node<E> getRight() {
            return right;
        }

        // update methods
        public void setElement(E e) {
            element = e;
        }

        public void setLeft(Node<E> leftChild) {
            left = leftChild;
        }

        public void setRight(Node<E> rightChild) {
            right = rightChild;
        }
    }

    //----------- end of nested Node class -----------
    protected Node<Symbol> root = null;     // root of the tree
    protected Node<Symbol> rootTreeLetter = null;

    /*
    * @return root Node of the tree (or null if tree is empty)
     */
    protected Node<Symbol> root() {
        return root;
    }

    protected Node<Symbol> rootLetter() {
        return rootTreeLetter;
    }

    /*
    * Verifies if the tree is empty
    * @return true if the tree is empty, false otherwise
     */
    public boolean isEmpty() {
        return root == null;
    }

    public boolean isEmptyLetter() {
        return rootTreeLetter == null;
    }

    // EX1
    public void insert(Symbol element) {
        Node current = root;
        String[] aux = element.getCode().split("(?!^)");
        for (int i = 0; i < aux.length; i++) {
            if (aux[i].equals(".")) {
                if (current.left == null) {
                    current.left = new Node(null, null, null);
                }
                current = current.left;
            } else if (aux[i].equals("_")) {
                if (current.right == null) {
                    current.right = new Node(null, null, null);
                }
                current = current.right;
            }
        }
        current.element = element;
    }

    public void insertLetter(Symbol element) {
        Node current = rootTreeLetter;
        String[] aux = element.getCode().split("(?!^)");
        for (int i = 0; i < aux.length; i++) {
            if (aux[i].equals(".")) {
                if (current.left == null) {
                    current.left = new Node(null, null, null);
                }
                current = current.left;
            } else if (aux[i].equals("_")) {
                if (current.right == null) {
                    current.right = new Node(null, null, null);
                }
                current = current.right;
            }
        }
        current.element = element;
    }

//#########################################################################
    /**
     * Returns a string representation of the tree. Draw the tree horizontally
     */
    public String toStringTree() {
        StringBuilder sb = new StringBuilder();
        toStringRec(root, 0, sb);
        return sb.toString();
    }

    public String toStringTreeLetter() {
        StringBuilder sb = new StringBuilder();
        toStringRec(rootTreeLetter, 0, sb);
        return sb.toString();
    }

    private void toStringRec(Node<Symbol> root, int level, StringBuilder sb) {
        if (root == null) {
            return;
        }
        toStringRec(root.getRight(), level + 1, sb);
        if (level != 0) {
            for (int i = 0; i < level - 1; i++) {
                sb.append("|\t");
            }
            sb.append("|-------" + root.getElement() + "\n");
        } else {
            sb.append(root.getElement() + "\n");
        }
        toStringRec(root.getLeft(), level + 1, sb);
    }

    public BST_Morse() {
        root = new Node<>(new Symbol(), null, null);
        rootTreeLetter = new Node<>(new Symbol(), null, null);
    }

    //EX 3
    public Node<Symbol> createTreeLetter() {
        SubTreeLetter(root);
        return rootTreeLetter;
    }

    private void SubTreeLetter(Node<Symbol> node) {
        addRight(node.getRight());
        addLeft(node.getLeft());
    }

    private void addLeft(Node<Symbol> node) {
        if (node == null) {
            return;
        }
        if (node.getElement().getTipo().equals("Letter")) {
            insertLetter(node.getElement());
        }
        addLeft(node.getLeft());
        addLeft(node.getRight());
    }

    private void addRight(Node<Symbol> node) {
        if (node == null) {
            return;
        }
        if (node.getElement().getTipo().equals("Letter")) {
            insertLetter(node.getElement());
        }
        addRight(node.getLeft());
        addRight(node.getRight());
    }

    // EX2
    public Symbol findSymbol(String code, Node root) {
        if (code.isEmpty()) {
            return null;
        }
        Symbol symb = null;
        // devide os digitos
        String[] element = code.split("");
        for (int i = 0; i < element.length; i++) {
            if (i < element.length) {
                // procura se o digito é um ponto
                if (element[i].equals(".")) {
                    // vai à esquerda buscar o simbolo
                    symb = (Symbol) root.getLeft().getElement();
                    // passa a root para a esquerda
                    root = root.getLeft();
                }
            }
            if (i < element.length) {
                // procura se o digito é uma linha
                if (element[i].equals("_")) {
                    // vai à direita buscar o simbolo
                    symb = (Symbol) root.getRight().getElement();
                    // passa a root para a direita
                    root = root.getRight();
                }
            }
        }
        // retorna o simbolo
        return symb;
    }

    // EX2
    public String descCode(String frase) {
        String descodificado = "";
        // separação das palavras
        String[] palavras = frase.split("/");
        String[] codigo = null;
        List<Integer> tamanhoPalavra = new ArrayList<>();
        List<String> code = new ArrayList<>();
        for (int i = 0; i < palavras.length; i++) {
            // Separação das letras
            codigo = palavras[i].split(" ");
            for (int j = 0; j < codigo.length; j++) {
                // add das letras à list
                code.add(codigo[j]);
            }
            // add tamanho da palavra à list
            tamanhoPalavra.add(code.size());
        }
        int j = 0;
        for (int i = 0; i < code.size(); i++) {
            if (j < tamanhoPalavra.size()) {
                // verificação se muda de palavra
                if (tamanhoPalavra.get(j) != i) {
                    // add letra à string final
                    descodificado += findSymbol(code.get(i), root());
                } else {
                    // add espaço e letra à string final
                    descodificado += " " + findSymbol(code.get(i), root());
                    j++;
                }
            }
        }
        // retorna frase
        return descodificado;
    }

    // EX4
    public String codCodeLetter(String frase) {
        String str = "";
        String codificado = "";
        String[] palavras = frase.split(" ");
        String[] letra = null;
        List<Integer> tamanhoPalavra = new ArrayList<>();
        List<String> letras = new ArrayList<>();
        for (int i = 0; i < palavras.length; i++) {
            // Separação das letras
            letra = palavras[i].split("");
            for (int j = 0; j < letra.length; j++) {
                // add das letras à list
                letras.add(letra[j]);
            }
            // add tamanho da palavra à list
            tamanhoPalavra.add(letras.size());
        }
        int j = 0;
        for (int i = 0; i < letras.size(); i++) {
            if (j < tamanhoPalavra.size()) {
                // verificação se muda de palavra
                if (tamanhoPalavra.get(j) != i) {
                    Symbol s = getSymbolByLetter(letras.get(i), rootLetter());
                    // add letra à string final
                    codificado += " " + findCode(s, rootLetter(), str);
                } else {
                    Symbol s = getSymbolByLetter(letras.get(i), rootLetter());
                    // add espaço e letra à string final
                    codificado += "/" + findCode(s, rootLetter(), str);
                    j++;
                }
            }
        }
        return codificado.substring(1);
    }

    // EX4
    protected String findCode(Symbol symb, Node<Symbol> root, String codigo_letra) {
        if (root == null) {
            return null;
        }
        String right;
        String left;
        if (root.getElement().equals(symb)) {
            return codigo_letra;
        } else {
            left = findCode(symb, root.getLeft(), codigo_letra + ".");
            right = findCode(symb, root.getRight(), codigo_letra + "_");

            if (left == null && right != null) {
                return right;
            } else if (left != null && right == null) {
                return left;
            } else {
                return null;
            }
        }
    }

    // EX5
    public String sameSequence(String a, String b) {
        String[] simbolo1 = codCodeBST(a).split("");
        String[] simbolo2 = codCodeBST(b).split("");
        String[] aux;
        if (simbolo1.length > simbolo2.length) {
            aux = simbolo1;
            simbolo1 = simbolo2;
            simbolo2 = aux;
        }
        String sequence = "";
        if (!simbolo1[0].equals(simbolo2[0])) {
            sequence = "Não existe sequência inicial comum aos dois caracteres indicados";
        }
        for (int i = 0; i < simbolo1.length; i++) {

            if (simbolo1[i].equals(simbolo2[i])) {
                sequence += simbolo1[i];
            } else {
                return sequence;
            }
        }
        return sequence;
    }

    // EX5
    public String codCodeBST(String frase) {
        String str = "";
        String codificado = "";
        String[] palavras = frase.split(" ");
        String[] letra = null;
        List<Integer> tamanhoPalavra = new ArrayList<>();
        List<String> letras = new ArrayList<>();
        for (int i = 0; i < palavras.length; i++) {
            // Separação das letras
            letra = palavras[i].split("");
            for (int j = 0; j < letra.length; j++) {
                // add das letras à list
                letras.add(letra[j]);
            }
            // add tamanho da palavra à list
            tamanhoPalavra.add(letras.size());
        }
        int j = 0;
        for (int i = 0; i < letras.size(); i++) {
            if (j < tamanhoPalavra.size()) {
                // verificação se muda de palavra
                if (tamanhoPalavra.get(j) != i) {
                    Symbol s = getSymbolByLetter(letras.get(i), root());
                    // add letra à string final
                    codificado += " " + findCode(s, root(), str);
                } else {
                    Symbol s = getSymbolByLetter(letras.get(i), root());
                    // add espaço e letra à string final
                    codificado += "/" + findCode(s, root(), str);
                    j++;
                }
            }
        }
        return codificado.substring(1);
    }

    // EX6
    public List<String> descodificacaoPalavras(List<String> codes, String tipo) {
        List<String> palavras = new ArrayList<>();
        String palavra = "";
        for (String code : codes) {
            String[] elemento = code.split(" ");
            for (int i = 0; i < elemento.length; i++) {
                String letra = descCode(elemento[i]);
                palavra += letra;
            }
            palavras.add(palavra);
            palavra = "";
        }
        return palavras;
    }

    // EX6
    public List<String> ordenacaoPorTipoSimbolo(List<String> codes, String tipo) {
        List<String> palavras = descodificacaoPalavras(codes, tipo);
        List<String> palavrasWithType = new ArrayList<>();
        List<Symbol> simbolos = new ArrayList<>();
        int cont = 0, aux = 0;
        for (String palavra : palavras) {
            String[] letras = palavra.split("");
            for (int i = 0; i < letras.length; i++) {
                simbolos.add(getSymbolByLetter(letras[i], root()));
                if (simbolos.get(aux).getTipo().equals(tipo)) {
                    cont++;
                }
                if (cont != 0 && i == letras.length - 1) {
                    palavrasWithType.add(palavra + "-" + cont);
                    cont = 0;
                }
                aux++;
            }
        }
        return ordenacao(palavrasWithType);
    }

    private Symbol getSymbolByLetter(String letra, Node<Symbol> node) {
        List<Symbol> letras = new ArrayList<>();
        preOrderSubtree(node, letras);
        for (int i = 1; i < letras.size(); i++) {
            if (letras.get(i).getSymbol().equals(letra)) {
                return letras.get(i);
            }
        }
        return null;
    }

    private void preOrderSubtree(Node<Symbol> node, List<Symbol> snapshot) {
        if (node == null) {
            return;
        }
        snapshot.add(node.getElement());
        preOrderSubtree(node.getLeft(), snapshot);
        preOrderSubtree(node.getRight(), snapshot);
    }

    private List<String> ordenacao(List<String> palavras) {
        String aux;
        for (int i = 0; i < palavras.size(); i++) {
            String[] palavra1 = palavras.get(i).split("-");
            for (int j = i + 1; j < palavras.size(); j++) {
                String[] palavra2 = palavras.get(j).split("-");
                if (Integer.parseInt(palavra1[1]) < Integer.parseInt(palavra2[1])) {
                    aux = palavras.get(i);
                    palavras.set(i, palavras.get(j));
                    palavras.set(j, aux);
                }
            }
        }
        return palavras;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.root);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BST_Morse other = (BST_Morse) obj;

        return true;
    }

}
