/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import model.Pair;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class PairTest {
    
    @Test
    public void newPairTest(){
        Pair p = new Pair("A","B");
        assertEquals(new Pair("A","B"), p);
    }
    
    @Test
    public void getETest(){
        Pair p = new Pair("A","B");
        assertEquals("A", p.getE());
    }
    
    @Test
    public void getSTest(){
        Pair p = new Pair("A","B");
        assertEquals("B", p.getS());
    }
    
    @Test
    public void setETest(){
        Pair p = new Pair("A","B");
        p.setE("C");
        assertEquals("C", p.getE());
    }
    
    @Test
    public void setSTest(){
        Pair p = new Pair("A","B");
        p.setS("D");
        assertEquals("D", p.getS());
    }
    
    @Test
    public void toStringTest(){
        Pair p = new Pair("A","B");
        assertEquals(p.toString(),"Pair{e=A, s=B}");
    }
    
    @Test
    public void hashCodeTest(){
        Pair p = new Pair("A","B");
        assertEquals(p.hashCode(),72234);
    }
    
}
