/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import java.util.ArrayList;
import java.util.List;
import model.Estacao;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class EstacaoTest {
    
    @Test
    public void newEstacao(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        assertEquals(e, new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228")));
    }
    
    @Test
    public void getNameTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        assertEquals("Trindade",e.getName());
    }
    
    @Test
    public void getLatitudeTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        assertEquals("41.09051",e.getLatitude().toString());
    }
    
    @Test
    public void getLongitudeTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        assertEquals("8.363228",e.getLongitude().toString());
    }
    
    @Test
    public void setNameTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setName("Lapa");
        assertNotEquals("Trindade",e.getName());
    }
    
    @Test
    public void setLatitudeTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setLatitude(Float.parseFloat("41.09052"));
        assertNotEquals("41.09051",e.getLatitude().toString());
    }
    
    @Test
    public void setLongitudeTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setLongitude(Float.parseFloat("8.363229"));
        assertNotEquals("8.363228",e.getLongitude().toString());
    }
    
    @Test
    public void getLinhaTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setLinha("1a");
        List<String> a = new ArrayList<>();
        a.add("1a");
        assertEquals(a,e.getLinha());
    }
    
    @Test
    public void getLinhaTest1(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setLinha("1a");
        e.setLinha("2a");
        List<String> a = new ArrayList<>();
        a.add("1a");
        a.add("2a");
        assertEquals(a,e.getLinha());
    }
    
    @Test
    public void getLinhaTest3(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        e.setLinha("1a");
        e.setLinha("1a");
        List<String> a = new ArrayList<>();
        a.add("1a");
        assertEquals(a,e.getLinha());
    }
    
    @Test
    public void hashCodeTest(){
        Estacao e = new Estacao("Trindade",Float.parseFloat("41.09051"),Float.parseFloat("8.363228"));
        assertEquals(1923940157,e.hashCode());
    }
}
