/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import model.Estacao;
import model.LeituraFicheiros;
import model.Metro;
import model.Pair;
import model.Percurso;
import static org.junit.Assert.*;
import org.junit.Test;
import projeto.Graph;

/**
 *
 * @author Utilizador
 */
public class MetroTest {
    
    public Metro newMetroTest(){
        return new Metro();
    }
    
    @Test
    public void lerEstacoesTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        Graph<Estacao, String> g = m.getMetro();
        assertEquals(299,g.numVertices());
    }
    
    @Test
    public void lerConexoesTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        Graph<Estacao, String> g = m.getMetro();
        assertEquals(712,g.numEdges());
    }
    
    @Test
    public void estacaoByNameTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        assertEquals(new Estacao("Abbesses",Float.parseFloat("2.33855"),Float.parseFloat("48.8843")),m.estacaoByName("Abbesses"));
    }
    
    @Test
    public void estacaoByNameTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        assertEquals(null,m.estacaoByName("Trindade"));
    }
    
    @Test
    public void checkEstacaoExisteTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        assertFalse(m.checkEstacaoExiste("Trindade"));
    }
    
    @Test
    public void checkEstacaoExisteTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        assertTrue(m.checkEstacaoExiste("Abbesses"));
    }
    
    @Test
    public void conexoTestGrafoPequeno() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao2(m);
        lf.leituraLinha2(m);
        lf.leituraConexao2(m);
        assertNull(m.conexo()); //Se for null, o grafo é conexo
    }
    
    //exercicio 2
    @Test
    public void conexoTestGrafo() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        assertNull(m.conexo()); //Se for null, o grafo é conexo
    }
    
    @Test
    public void percursoMinimoTempoEntreDuasEstacoesTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao2(m);
        lf.leituraLinha2(m);
        lf.leituraConexao2(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("2");
        linhas.add("2");
        pair.add(new Pair(m.estacaoByName("Lapa"),0.0));
        pair.add(new Pair(m.estacaoByName("Trindade"),2.0));
        pair.add(new Pair(m.estacaoByName("Aliados"),1.0));
        pair.add(new Pair(m.estacaoByName("Fanzeres"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p,m.caminhoMinimoEntreDuasEstacoes("Lapa", "Fanzeres", 10));
    }
    
    @Test
    public void percursoMinimoEstacoesEntreDuasEstacoesTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao2(m);
        lf.leituraLinha2(m);
        lf.leituraConexao2(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("2");
        linhas.add("2");
        pair.add(new Pair(m.estacaoByName("Lapa"),0.0));
        pair.add(new Pair(m.estacaoByName("Trindade"),2.0));
        pair.add(new Pair(m.estacaoByName("Aliados"),1.0));
        pair.add(new Pair(m.estacaoByName("Fanzeres"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        Percurso pp = m.caminhoMinimoEstacoes("Lapa", "Fanzeres", 10);
        assertEquals(p,pp);
    }
    
    @Test
    public void percursoMinimoLinhasEntreDuasEstacoesTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao2(m);
        lf.leituraLinha2(m);
        lf.leituraConexao2(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        pair.add(new Pair(m.estacaoByName("Lapa"),0.0));
        pair.add(new Pair(m.estacaoByName("Trindade"),2.0));
        pair.add(new Pair(m.estacaoByName("Bolhao"),5.0));
        pair.add(new Pair(m.estacaoByName("Dragao"),2.0));
        pair.add(new Pair(m.estacaoByName("Fanzeres"),1.0));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p,m.caminhoMinimoLinhas("Lapa", "Fanzeres", 10));
    }
    
    @Test
    public void percursoMinimoTempoEntreDuasEstacoesComIntermediasTest() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao2(m);
        lf.leituraLinha2(m);
        lf.leituraConexao2(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("2");
        linhas.add("3");
        linhas.add("1");
        pair.add(new Pair(m.estacaoByName("Lapa"),0.0));
        pair.add(new Pair(m.estacaoByName("Trindade"),2.0));
        pair.add(new Pair(m.estacaoByName("Aliados"),1.0));
        pair.add(new Pair(m.estacaoByName("Dragao"),1.0));
        pair.add(new Pair(m.estacaoByName("Fanzeres"),1.0));
        Percurso p = new Percurso(linhas,pair,10);
        List<Estacao> intermedias = new ArrayList<>();
        intermedias.add(m.estacaoByName("Aliados"));
        intermedias.add(m.estacaoByName("Dragao"));
        Percurso pp = m.caminhoMinimoEstacoesIntermedias("Lapa", "Fanzeres", 10, intermedias);
        assertEquals(p,pp);
    }
    
    
    //exercico 4
    @Test
    public void percursoMinimoTempoEntreDuasEstacoesTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");    
        linhas.add("8");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        pair.add(new Pair(m.estacaoByName("La Defense"),0.0));
        pair.add(new Pair(m.estacaoByName("Esplanade de La Defense"),1.0));
        pair.add(new Pair(m.estacaoByName("Pont de Neuilly"),1.0));
        pair.add(new Pair(m.estacaoByName("Les Sablons"),2.0));
        pair.add(new Pair(m.estacaoByName("Porte Maillot"),1.0));
        pair.add(new Pair(m.estacaoByName("Argentine"),2.0));
        pair.add(new Pair(m.estacaoByName("Charles de Gaulle Etoile"),1.0));
        pair.add(new Pair(m.estacaoByName("George V"),1.0));
        pair.add(new Pair(m.estacaoByName("Franklin Roosevelt"),1.0));
        pair.add(new Pair(m.estacaoByName("Champs Elysees Clemenceau"),2.0));
        pair.add(new Pair(m.estacaoByName("Concorde"),1.0));
        pair.add(new Pair(m.estacaoByName("Madeleine"),1.0));
        pair.add(new Pair(m.estacaoByName("Pyramides"),2.0));
        pair.add(new Pair(m.estacaoByName("Chatelet"),2.0));
        pair.add(new Pair(m.estacaoByName("Gare de Lyon"),3.0));
        pair.add(new Pair(m.estacaoByName("Bercy"),2.0));
        pair.add(new Pair(m.estacaoByName("Cour Saint Emillion"),1.0));
        pair.add(new Pair(m.estacaoByName("Bibliotheque Francois Mitterrand"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p,m.caminhoMinimoEntreDuasEstacoes("La Defense", "Bibliotheque Francois Mitterrand", 10));
    }
    
    //exercicio 5
    @Test
    public void percursoMinimoEstacoesEntreDuasEstacoesTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");    
        linhas.add("8");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        linhas.add("14");    
        pair.add(new Pair(m.estacaoByName("La Defense"),0.0));
        pair.add(new Pair(m.estacaoByName("Esplanade de La Defense"),1.0));
        pair.add(new Pair(m.estacaoByName("Pont de Neuilly"),1.0));
        pair.add(new Pair(m.estacaoByName("Les Sablons"),2.0));
        pair.add(new Pair(m.estacaoByName("Porte Maillot"),1.0));
        pair.add(new Pair(m.estacaoByName("Argentine"),2.0));
        pair.add(new Pair(m.estacaoByName("Charles de Gaulle Etoile"),1.0));
        pair.add(new Pair(m.estacaoByName("George V"),1.0));
        pair.add(new Pair(m.estacaoByName("Franklin Roosevelt"),1.0));
        pair.add(new Pair(m.estacaoByName("Champs Elysees Clemenceau"),2.0));
        pair.add(new Pair(m.estacaoByName("Concorde"),1.0));
        pair.add(new Pair(m.estacaoByName("Madeleine"),1.0));
        pair.add(new Pair(m.estacaoByName("Pyramides"),2.0));
        pair.add(new Pair(m.estacaoByName("Chatelet"),2.0));
        pair.add(new Pair(m.estacaoByName("Gare de Lyon"),3.0));
        pair.add(new Pair(m.estacaoByName("Bercy"),2.0));
        pair.add(new Pair(m.estacaoByName("Cour Saint Emillion"),1.0));
        pair.add(new Pair(m.estacaoByName("Bibliotheque Francois Mitterrand"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        Percurso pp = m.caminhoMinimoEstacoes("La Defense", "Bibliotheque Francois Mitterrand", 10);
        assertEquals(p,pp);
    }
    
    //exercicio 6
    @Test
    public void percursoMinimoLinhasEntreDuasEstacoesTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("14");
        linhas.add("14");
        linhas.add("14");
        linhas.add("14");
        pair.add(new Pair(m.estacaoByName("La Defense"),0.0));
        pair.add(new Pair(m.estacaoByName("Esplanade de La Defense"),1.0));
        pair.add(new Pair(m.estacaoByName("Pont de Neuilly"),1.0));
        pair.add(new Pair(m.estacaoByName("Les Sablons"),2.0));
        pair.add(new Pair(m.estacaoByName("Porte Maillot"),1.0));
        pair.add(new Pair(m.estacaoByName("Argentine"),2.0));
        pair.add(new Pair(m.estacaoByName("Charles de Gaulle Etoile"),1.0));
        pair.add(new Pair(m.estacaoByName("George V"),1.0));
        pair.add(new Pair(m.estacaoByName("Franklin Roosevelt"),1.0));
        pair.add(new Pair(m.estacaoByName("Champs Elysees Clemenceau"),2.0));
        pair.add(new Pair(m.estacaoByName("Concorde"),1.0));
        pair.add(new Pair(m.estacaoByName("Tuileries"),2.0));
        pair.add(new Pair(m.estacaoByName("Palais Royal Musee du Louvre"),1.0));
        pair.add(new Pair(m.estacaoByName("Louvre Rivoli"),1.0));
        pair.add(new Pair(m.estacaoByName("Chatelet"),1.0));
        pair.add(new Pair(m.estacaoByName("Gare de Lyon"),3.0));
        pair.add(new Pair(m.estacaoByName("Bercy"),2.0));
        pair.add(new Pair(m.estacaoByName("Cour Saint Emillion"),1.0));
        pair.add(new Pair(m.estacaoByName("Bibliotheque Francois Mitterrand"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p,m.caminhoMinimoLinhas("La Defense", "Bibliotheque Francois Mitterrand", 10));
    }
    
    //exercicio 7
    @Test
    public void percursoMinimoTempoEntreDuasEstacoesComIntermediasTest2() throws FileNotFoundException{
        Metro m = newMetroTest();
        LeituraFicheiros lf = new LeituraFicheiros();
        lf.leituraEstacao(m);
        lf.leituraLinha(m);
        lf.leituraConexao(m);
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("1");
        linhas.add("8");
        linhas.add("8");
        linhas.add("8");
        linhas.add("8");
        linhas.add("8");
        linhas.add("8");
        linhas.add("8");
        linhas.add("5");
        linhas.add("9");
        linhas.add("9");
        linhas.add("9");
        linhas.add("9");
        linhas.add("9");
        linhas.add("5");
        linhas.add("11");
        linhas.add("11");
        linhas.add("11");
        linhas.add("4");
        linhas.add("4");
        linhas.add("4");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("10");
        linhas.add("4");
        linhas.add("4");
        linhas.add("4");
        linhas.add("14");
        linhas.add("14");
        linhas.add("14");
        pair.add(new Pair(m.estacaoByName("La Defense"),0.0));
        pair.add(new Pair(m.estacaoByName("Esplanade de La Defense"),1.0));
        pair.add(new Pair(m.estacaoByName("Pont de Neuilly"),1.0));
        pair.add(new Pair(m.estacaoByName("Les Sablons"),2.0));
        pair.add(new Pair(m.estacaoByName("Porte Maillot"),1.0));
        pair.add(new Pair(m.estacaoByName("Argentine"),2.0));
        pair.add(new Pair(m.estacaoByName("Charles de Gaulle Etoile"),1.0));
        pair.add(new Pair(m.estacaoByName("George V"),1.0));
        pair.add(new Pair(m.estacaoByName("Franklin Roosevelt"),1.0));
        pair.add(new Pair(m.estacaoByName("Champs Elysees Clemenceau"),2.0));
        pair.add(new Pair(m.estacaoByName("Concorde"),1.0));
        pair.add(new Pair(m.estacaoByName("Madeleine"),1.0));
        pair.add(new Pair(m.estacaoByName("Opera"),2.0));
        pair.add(new Pair(m.estacaoByName("Richelieu Drouot"),1.0));
        pair.add(new Pair(m.estacaoByName("Grands Boulevards"),1.0));
        pair.add(new Pair(m.estacaoByName("Bonne Nouvelle"),1.0));
        pair.add(new Pair(m.estacaoByName("Strasbourg St Denis"),1.0));
        pair.add(new Pair(m.estacaoByName("Republique"),2.0));
        pair.add(new Pair(m.estacaoByName("Oberkampf"),1.0));
        pair.add(new Pair(m.estacaoByName("Saint Amboise"),2.0));
        pair.add(new Pair(m.estacaoByName("Voltaire"),1.0));
        pair.add(new Pair(m.estacaoByName("Voltaire"),0.0));
        pair.add(new Pair(m.estacaoByName("Saint Amboise"),1.0));
        pair.add(new Pair(m.estacaoByName("Oberkampf"),2.0));
        pair.add(new Pair(m.estacaoByName("Republique"),1.0));
        pair.add(new Pair(m.estacaoByName("Arts et Metiers"),1.0));
        pair.add(new Pair(m.estacaoByName("Rambuteau"),1.0));
        pair.add(new Pair(m.estacaoByName("Chatelet"),3.0));
        pair.add(new Pair(m.estacaoByName("Cite"),1.0));
        pair.add(new Pair(m.estacaoByName("Saint Michel"),1.0));
        pair.add(new Pair(m.estacaoByName("Odeon"),1.0));
        pair.add(new Pair(m.estacaoByName("Mabillon"),2.0));
        pair.add(new Pair(m.estacaoByName("Sevres Babylone"),1.0));
        pair.add(new Pair(m.estacaoByName("Vaneau"),1.0));
        pair.add(new Pair(m.estacaoByName("Segur"),1.0));
        pair.add(new Pair(m.estacaoByName("Segur"),0.0));
        pair.add(new Pair(m.estacaoByName("Duroc"),1.0));
        pair.add(new Pair(m.estacaoByName("Vaneau"),2.0));
        pair.add(new Pair(m.estacaoByName("Sevres Babylone"),1.0));
        pair.add(new Pair(m.estacaoByName("Mabillon"),1.0));
        pair.add(new Pair(m.estacaoByName("Odeon"),2.0));
        pair.add(new Pair(m.estacaoByName("Saint Michel"),1.0));
        pair.add(new Pair(m.estacaoByName("Cite"),1.0));
        pair.add(new Pair(m.estacaoByName("Chatelet"),1.0));
        pair.add(new Pair(m.estacaoByName("Gare de Lyon"),3.0));
        pair.add(new Pair(m.estacaoByName("Bercy"),2.0));
        pair.add(new Pair(m.estacaoByName("Bibliotheque Francois Mitterrand"),2.0));
        Percurso p = new Percurso(linhas,pair,10);
        List<Estacao> intermedias = new ArrayList<>();
        intermedias.add(m.estacaoByName("Voltaire"));
        intermedias.add(m.estacaoByName("Segur"));
        Percurso pp = m.caminhoMinimoEstacoesIntermedias("La Defense", "Bibliotheque Francois Mitterrand", 10, intermedias);
        assertEquals(p,pp);
    }
}
