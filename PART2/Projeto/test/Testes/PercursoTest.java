/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import java.util.ArrayList;
import java.util.List;
import model.Pair;
import model.Percurso;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class PercursoTest {
    
    @Test
    public void newPercursoTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p,new Percurso(linhas,pair,10));
    }
    
    @Test
    public void getLinhasTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(linhas,p.getLinha());
    }
    
    @Test
    public void getPairTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(pair, p.getPar());
    }
    
    @Test
    public void getInstanteInicialTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(10, p.getInstanteInicio());
    }
    
    @Test
    public void setLinhasTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        List<String> linhas1 = new ArrayList<>();
        linhas1.add("3");
        linhas1.add("1");
        p.setLinha(linhas1);
        assertEquals(linhas1,p.getLinha());
    }
    
    @Test
    public void setPairTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        List<Pair> pair1 = new ArrayList<>();
        pair1.add(new Pair("B","C"));
        p.setPar(pair1);
        assertEquals(pair1, p.getPar());
    }
    
    @Test
    public void setInstanteInicialTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A","B"));
        Percurso p = new Percurso(linhas,pair,10);
        p.setInstanteInicio(0);
        assertEquals(0, p.getInstanteInicio());
    }
    
    @Test
    public void tempoFinalViagemTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A",Double.parseDouble("2")));
        Percurso p = new Percurso(linhas,pair,10);
        boolean aux = false;
        if (p.tempoFinalViagem() == 12.0)
            aux = true;
        assertTrue(aux);
    }
    
    @Test
    public void toStringTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A",Double.parseDouble("2")));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(p.toString(),"Percurso{linha=[1, 2], par=[Pair{e=A, s=2.0}], instanteInicio=10}");
    }
    
    @Test
    public void hashCodeTest(){
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        linhas.add("1");
        linhas.add("2");
        pair.add(new Pair("A",Double.parseDouble("2")));
        Percurso p = new Percurso(linhas,pair,10);
        assertEquals(1093661782,p.hashCode());
    }
}
