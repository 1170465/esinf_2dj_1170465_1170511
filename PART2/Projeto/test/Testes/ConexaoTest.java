/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import model.Conexao;
import model.Estacao;
import model.Metro;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class ConexaoTest {
    
    @Test
    public void newConexaoTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals(new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2),c);
    }
    
    @Test
    public void getEorigemTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals(m.estacaoByName("Lapa"),c.geteOrig());
    }
    
    @Test
    public void getEdestinoTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals(m.estacaoByName("Trindade"),c.geteDest());
    }
    
    @Test
    public void getLinhaTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals("1",c.getLinha());
    }
    
    @Test
    public void getTimeTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals(2,c.getTime());
    }
    
    
    @Test
    public void setEorigemTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        Estacao e3 = new Estacao("IPO",Float.parseFloat("3.33333"),Float.parseFloat("4.44444"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        m.addEstacao(e3);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        c.seteOrig(e3);
        assertEquals(m.estacaoByName("IPO"),c.geteOrig());
    }
    
    @Test
    public void setEdestinoTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        Estacao e3 = new Estacao("IPO",Float.parseFloat("3.33333"),Float.parseFloat("4.44444"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        m.addEstacao(e3);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        c.seteDest(e3);
        assertEquals(m.estacaoByName("IPO"),c.geteDest());
    }
    
    @Test
    public void setLinhaTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        c.setLinha("2");
        assertEquals("2",c.getLinha());
    }
    
    @Test
    public void setTimeTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        c.setTime(1);
        assertEquals(1,c.getTime());
    }
    
    @Test
    public void toStringTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals("Conexao{eOrig=Estacao{name=Lapa, latitude=2.32652, longitude=48.82825}, eDest=Estacao{name=Trindade, latitude=2.33855, longitude=48.8843}, linha=1, time=2}", c.toString());
    }
    
    @Test
    public void hashCodeTest(){
        Metro m = new Metro();
        Estacao e1 = new Estacao("Lapa",Float.parseFloat("2.32652"),Float.parseFloat("48.82825"));
        Estacao e2 = new Estacao("Trindade",Float.parseFloat("2.33855"),Float.parseFloat("48.8843"));
        m.addEstacao(e1);
        m.addEstacao(e2);
        Conexao c = new Conexao(m.estacaoByName("Lapa"),m.estacaoByName("Trindade"),"1",2);
        assertEquals(-1778238646,c.hashCode());
    }
}
