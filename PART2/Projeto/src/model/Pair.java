/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author Utilizador
 */
public class Pair<E,S> {
    
    private E e;
    private S s;

    public Pair(E e, S s) {
        this.e = e;
        this.s = s;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        if (!equals(e))
        this.e = e;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        if (!equals(e))
        this.s = s;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.e);
        hash = 97 * hash + Objects.hashCode(this.s);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair<?, ?> other = (Pair<?, ?>) obj;
        if (!Objects.equals(this.e, other.e)) {
            return false;
        }
        if (!Objects.equals(this.s, other.s)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pair{" + "e=" + e + ", s=" + s + '}';
    }
}
