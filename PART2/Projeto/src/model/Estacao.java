/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Utilizador
 */
public class Estacao {
    
    private String name;
    private Float latitude;
    private Float longitude;
    List<String> linha = new ArrayList<>();
    
    public Estacao(String nome,Float latitude,Float longitude){
        this.name = nome;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!equals(name))
        this.name = name;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        if (!equals(latitude))
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        if (!equals(longitude))
        this.longitude = longitude;
    }

    public List<String> getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        if (this.linha.isEmpty()){
            this.linha.add(linha);
        }
        if (!this.linha.contains(linha)){
            this.linha.add(linha);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.latitude);
        hash = 53 * hash + Objects.hashCode(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estacao{" + "name=" + name + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }
}
