/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import projeto.Edge;
import projeto.Graph;
import projeto.GraphAlgorithms;

/**
 *
 * @author Utilizador
 */
public class Metro {

    private Graph<Estacao, String> metro;
    private ArrayList componentes;

    public Metro() {
        metro = new Graph<>(false);
    }

    public Graph<Estacao, String> getMetro() {
        return metro;
    }

    public ArrayList getComponentes() {
        return componentes;
    }

    public void addEstacao(Estacao est) {
        metro.insertVertex(est);
    }

    public void addConexao(Conexao c) {
        metro.insertEdge(c.geteOrig(), c.geteDest(), c.getLinha(), c.getTime());
    }

    public Estacao estacaoByName(String name) {
        Estacao[] names = metro.allkeyVerts();
        for (Estacao nome : names) {
            if (nome.getName().equals(name)) {
                return nome;
            }
        }
        return null;
    }

    public boolean checkEstacaoExiste(String e) {
        for (Estacao est : metro.vertices()) {
            if (est.equals(estacaoByName(e))) {
                return true;
            }
        }
        return false;
    }

    // Ex. 2
    // Método para verificar se o grafo é conexo
    public LinkedList conexo() {
        return metro.isConnected(metro, componentes);
    }

    // Ex. 4
    // Encontrar o caminho mínimo entre duas estações, quanto ao número de 
    // estações.
    // Retorna uma instância da classe Percurso.
    public Percurso caminhoMinimoEstacoes(String estInicio, String estDestino, int instanteInicial) {
        List<Pair> pair = numeroEstacoes(estInicio, estDestino);
        List<String> linhas = linhasMenosEstacoesPossivel(estInicio, estDestino);
        return new Percurso(linhas, pair, instanteInicial);
    }

    // Ex. 5
    // Encontrar o caminho mínimo entre duas estações, quanto ao tempo a 
    // percorrer.
    // Retorna uma instância da classe Percurso.
    public Percurso caminhoMinimoEntreDuasEstacoes(String estInicio, String estDestino, int instanteInicial) {
        List<String> linhas = linhasMenosTempoPossivel(estInicio, estDestino);
        List<Pair> pair = estacoesTime(estInicio, estDestino);
        return new Percurso(linhas, pair, instanteInicial);
    }

    // Ex. 6
    // Encontrar o caminho mínimo entre duas estações, quanto ao número de 
    // mudanças de linha.
    // Retorna uma instância da classe Percurso.
    public Percurso caminhoMinimoLinhas(String estInicio, String estDestino, int instanteInicial) {
        List<Pair> pair = numeroLinhas(estInicio, estDestino);
        List<String> linhas = linhasMenosLinhasPossivel(estInicio, estDestino);
        return new Percurso(linhas, pair, instanteInicial);
    }

    // Para obter o número de estações resolvemos colocar o peso de todos as 
    // estações (edges) igual a 1 para quando utilizássemos o shortestPath, ser
    // escolhido o caminho com menos número de estações.
    private List<Pair> numeroEstacoes(String estInicio, String estDestino) {
        List<Pair> pair = new ArrayList<>();
        List<Double> peso = new ArrayList<>();
        for (Edge e : metro.edges()) {
            peso.add(e.getWeight());
            e.setWeight(1.0);
        }
        if (checkEstacaoExiste(estInicio) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estInicio), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    Pair p = new Pair(e, ed.getWeight());
                    pair.add(p);
                }
                Pair p = new Pair(e2, 0.0);
                pair.add(p);
                Collections.reverse(pair);
                int i = 0, c = 1;
                while (c < pair.size()) {
                    for (Edge e : metro.edges()) {
                        if (pair.get(c - 1).getE() == e.getVOrig() && pair.get(c).getE() == e.getVDest()) {
                            pair.get(c).setS(peso.get(i));
                            c++;
                            e.setWeight(peso.get(i));
                            i++;
                            break;
                        }
                        e.setWeight(peso.get(i));
                        i++;
                    }
                    i = 0;
                }
                return pair;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<String> linhasMenosEstacoesPossivel(String estInicio, String estDestino) {
        List<Double> peso = new ArrayList<>();
        for (Edge e : metro.edges()) {
            peso.add(e.getWeight());
            e.setWeight(1.0);
        }
        if (checkEstacaoExiste(estInicio) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estInicio), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                List<String> linhas = new ArrayList<>();
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                String linha = null;
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    linha = (String) ed.getElement();
                    linhas.add(linha);
                }
                linhas.add(linha);
                Collections.reverse(linhas);
                int i = 0;
                for (Edge e : metro.edges()) {
                    e.setWeight(peso.get(i));
                    i++;
                }
                return linhas;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<String> linhasMenosTempoPossivel(String estInicio, String estDestino) {
        if (checkEstacaoExiste(estInicio) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estInicio), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                List<String> linhas = new ArrayList<>();
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                String linha = null;
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    linha = (String) ed.getElement();
                    linhas.add(linha);
                }
                linhas.add(linha);
                Collections.reverse(linhas);
                return linhas;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private List<Pair> estacoesTime(String estInicio, String estDestino) {
        List<Pair> pair = new ArrayList<>();
        if (checkEstacaoExiste(estInicio) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estInicio), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    Pair p = new Pair(e, ed.getWeight());
                    pair.add(p);
                }
                Pair p = new Pair(e2, 0.0);
                pair.add(p);
                Collections.reverse(pair);
                return pair;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<Pair> numeroLinhas(String estOrigem, String estDestino) {
        List<Pair> pair = new ArrayList<>();
        List<Double> peso = new ArrayList<>();
        for (Edge e : metro.edges()) {
            peso.add(e.getWeight());
            e.setWeight(1.0);
        }
        menorLinhasPossivel(estOrigem);
        if (checkEstacaoExiste(estOrigem) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estOrigem), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    Pair p = new Pair(e, ed.getWeight());
                    pair.add(p);
                }
                Pair p = new Pair(e2, 0.0);
                pair.add(p);
                Collections.reverse(pair);
                int i = 0, c = 1;
                while (c < pair.size()) {
                    for (Edge e : metro.edges()) {
                        if (pair.get(c - 1).getE() == e.getVOrig() && pair.get(c).getE() == e.getVDest()) {
                            pair.get(c).setS(peso.get(i));
                            c++;
                            e.setWeight(peso.get(i));
                            i++;
                            break;
                        }
                        e.setWeight(peso.get(i));
                        i++;
                    }
                    i = 0;
                }
                return pair;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<String> linhasMenosLinhasPossivel(String estInicio, String estDestino) {
        List<Double> peso = new ArrayList<>();
        for (Edge e : metro.edges()) {
            peso.add(e.getWeight());
            e.setWeight(1.0);
        }
        menorLinhasPossivel(estInicio);
        if (checkEstacaoExiste(estInicio) && checkEstacaoExiste(estDestino)) {
            LinkedList<Estacao> listEstacoes = new LinkedList<>();
            GraphAlgorithms.shortestPath(metro, estacaoByName(estInicio), estacaoByName(estDestino), listEstacoes);
            if (!listEstacoes.isEmpty()) {
                List<String> linhas = new ArrayList<>();
                Iterator it = listEstacoes.iterator();
                Iterator it2 = listEstacoes.iterator();
                it2.next();
                String linha = null;
                Estacao e2 = null;
                while (it2.hasNext()) {
                    e2 = (Estacao) it2.next();
                    Estacao e = (Estacao) it.next();
                    Edge ed = metro.getEdge(e, e2);
                    linha = (String) ed.getElement();
                    linhas.add(linha);
                }
                linhas.add(linha);
                Collections.reverse(linhas);
                int i = 0;
                for (Edge e : metro.edges()) {
                    e.setWeight(peso.get(i));
                    i++;
                }
                return linhas;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public void menorLinhasPossivel(String estOrigem) {
        LinkedList<Estacao> estacoes = GraphAlgorithms.DepthFirstSearch(metro, estacaoByName(estOrigem));
        Iterable<Edge<Estacao, String>> edges = metro.outgoingEdges(estacaoByName(estOrigem));
        Iterator i = estacoes.iterator();
        int count = 0;
        while (estacoes.size() - 1 > count) {
            String linha = edges.iterator().next().getElement();
            for (Edge e : edges) {
                if (!e.getElement().equals(linha)) {
                    e.setWeight(e.getWeight() + 1.0);
                }
            }
            count++;
            if (estacoes.size() - 1 > count) {
                edges = metro.outgoingEdges((Estacao) i.next());
            }
        }
    }

    // Ex. 7
    // Encontrar o caminho mínimo entre duas estações, quanto ao tempo a 
    // percorrer e, passando obrigatoriamente por todas as estações intermédias.
    // Retorna uma instância da classe Percurso.
    public Percurso caminhoMinimoEstacoesIntermedias(String origem, String destino, int instanteInicial, List<Estacao> lst) {
        List<String> linhas = new ArrayList<>();
        List<Pair> pair = new ArrayList<>();
        for (int a = 0; a <= lst.size(); a++) {
            //ultima estacao intermedia e estacao destino
            if (a == lst.size()) {
                for (String linha : linhasMenosTempoPossivel(lst.get(lst.size() - 1).getName(), destino)) {
                    linhas.add(linha);
                }
                //remove para n haver repeticao
                linhas.remove(linhas.size() - 2);
                for (Pair p : estacoesTime(lst.get(lst.size() - 1).getName(), destino)) {
                    pair.add(p);
                }
                //remove para n haver repeticao
                pair.remove(linhas.size() - 1);
            }
            //estacao origem e primeira estacao intermédia
            if (a == 0) {
                for (String linha : linhasMenosTempoPossivel(origem, lst.get(a).getName())) {
                    linhas.add(linha);
                }
                for (Pair p : estacoesTime(origem, lst.get(a).getName())) {
                    pair.add(p);
                }
            }
            //2 estacoes intermedias
            if (a != 0 && a != lst.size()) {
                for (String linha : linhasMenosTempoPossivel(lst.get(a - 1).getName(), lst.get(a).getName())) {
                    linhas.add(linha);

                }
                //remove para n haver repeticao
                linhas.remove(linhas.size() - 1);
                for (Pair p : estacoesTime(lst.get(a - 1).getName(), lst.get(a).getName())) {
                    pair.add(p);
                }
                //remove para n haver repetição 
                pair.remove(linhas.size() - 1);
            }
        }
        return new Percurso(linhas, pair, instanteInicial);
    }
}
