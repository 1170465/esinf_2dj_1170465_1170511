/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author Utilizador
 */
public class Conexao {
    
    private Estacao eOrig;
    private Estacao eDest;
    private String linha;
    private int time;

    public Conexao(Estacao eOrig, Estacao eDest, String linha, int time) {
        this.eOrig = eOrig;
        this.eDest = eDest;
        this.linha = linha;
        this.time = time;
    }

    public Estacao geteOrig() {
        return eOrig;
    }

    public void seteOrig(Estacao eOrig) {
        if (!equals(eOrig))
        this.eOrig = eOrig;
    }

    public Estacao geteDest() {
        return eDest;
    }

    public void seteDest(Estacao eDest) {
        if (!equals(eOrig))
        this.eDest = eDest;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        if (!equals(eOrig))
        this.linha = linha;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        if (!equals(eOrig))
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.eOrig);
        hash = 59 * hash + Objects.hashCode(this.eDest);
        hash = 59 * hash + Objects.hashCode(this.linha);
        hash = 59 * hash + this.time;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conexao other = (Conexao) obj;
        if (this.time != other.time) {
            return false;
        }
        if (!Objects.equals(this.linha, other.linha)) {
            return false;
        }
        if (!Objects.equals(this.eOrig, other.eOrig)) {
            return false;
        }
        if (!Objects.equals(this.eDest, other.eDest)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Conexao{" + "eOrig=" + eOrig + ", eDest=" + eDest + ", linha=" + linha + ", time=" + time + '}';
    } 
}
