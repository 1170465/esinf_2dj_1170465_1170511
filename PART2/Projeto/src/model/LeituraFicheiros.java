/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Utilizador
 */
public class LeituraFicheiros {
    
    public void leituraEstacao(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("coordinates.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                m.addEstacao(new Estacao(temp[0],Float.parseFloat(temp[1]),Float.parseFloat(temp[2])));
            }
        }
        in.close();
    }
    
    public void leituraLinha(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("lines_and_stations.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                Estacao e = m.estacaoByName(temp[1]);
                e.setLinha(temp[0]);
            }
        }
        in.close();
    }
    
    public void leituraConexao(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("connections.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                Estacao e1 = m.estacaoByName(temp[1]);
                Estacao e2 = m.estacaoByName(temp[2]);
                for (String line : e1.getLinha()){
                    if (line.equals(temp[0])){
                        Conexao c = new Conexao(e1,e2,line,Integer.parseInt(temp[3]));
                        m.addConexao(c);
                    }
                }
            }
        }
        in.close();
    }
    
    
    public void leituraEstacao2(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("Estacao.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                m.addEstacao(new Estacao(temp[0],Float.parseFloat(temp[1]),Float.parseFloat(temp[2])));
            }
        }
        in.close();
    }
    
    public void leituraLinha2(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("linhas.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                Estacao e = m.estacaoByName(temp[1]);
                e.setLinha(temp[0]);
            }
        }
        in.close();
    }
    
    public void leituraConexao2(Metro m) throws FileNotFoundException{
        Scanner in = new Scanner(new FileReader("connect.csv"));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.length() > 0) {
                String temp[] = linha.split(";");
                Estacao e1 = m.estacaoByName(temp[1]);
                Estacao e2 = m.estacaoByName(temp[2]);
                for (String line : e1.getLinha()){
                    if (line.equals(temp[0])){
                        Conexao c = new Conexao(e1,e2,line,Integer.parseInt(temp[3]));
                        m.addConexao(c);
                    }
                }
            }
        }
        in.close();
    }
}
