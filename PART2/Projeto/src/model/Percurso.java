/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Utilizador
 */
public class Percurso {
    
    private List<String> linha = new ArrayList<>();
    private List<Pair> par = new ArrayList<>();
    private int instanteInicio;

    public Percurso(List<String> linha, List<Pair> par ,int instanteInicial) {
        this.linha = linha;
        this.par = par;
        this.instanteInicio = instanteInicial;
    }

    public List<String> getLinha() {
        return linha;
    }

    public void setLinha(List<String> linha) {
        this.linha = linha;
    }

    public List<Pair> getPar() {
        return par;
    }

    public void setPar(List<Pair> par) {
        this.par = par;
    }

    public int getInstanteInicio() {
        return instanteInicio;
    }

    public void setInstanteInicio(int instanteInicio) {
        this.instanteInicio = instanteInicio;
    }
    
    public Double tempoFinalViagem(){
        double tempo = this.instanteInicio;
        for (Pair p : par){
            tempo += Double.parseDouble(p.getS().toString());
        }
        return tempo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.linha);
        hash = 73 * hash + Objects.hashCode(this.par);
        hash = 73 * hash + this.instanteInicio;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Percurso other = (Percurso) obj;
        if (this.instanteInicio != other.instanteInicio) {
            return false;
        }
        if (!Objects.equals(this.linha, other.linha)) {
            return false;
        }
        if (!Objects.equals(this.par, other.par)) {
            return false;
        }
        return true;
    }

    

    

    @Override
    public String toString() {
        return "Percurso{" + "linha=" + linha + ", par=" + par + ", instanteInicio=" + instanteInicio + '}';
    }
}
